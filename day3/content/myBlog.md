Title: Introduction to Pelican
Date: 2024 - 05 - 08
Category: pelican
Tags: pelican, publishing
Slug: my-Blog
Authors: Chitti thalli kaja
Summary: Short intro of pelican


Pelican is a static site generator , written in python allows you to create websites ,composed of text files,mark down files, reStructuredText and HTML files.

There is no need to worry about data bases or sever-side programming.

-pelican also refers as a framework -It generates HTML folders containing the code to create web pages.

How to work with pelican in Ubuntu?

-Install pelican 
    -go to terminal and type
        sudo apt install pelican

~ to upgrade pelican
    sudo apt -yes dist-upgrade

~to Kickstart with pelican
    pelican-quickstart

You are prompted to answer some questions about author,title,etc details of your website.

Now you are informed about the location of files created for your website in your computer .
Now go to that directory/folder from your terminal by using the command 
     cd directoryName
now use any of the editors to edit the files 
---To use VS code ,type the command 
      code .
now you are directed to vs code and edit your website there.
---After editing your file in vs code ,now publish your site ,to do that use the command 
        pelican /path/to/your/file/