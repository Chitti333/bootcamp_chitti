"""Problem statement : Implement the algorithm taht computes check digit for ISBN13
What is an ISBN13?
   International Standard Book Number can be 10 or 13
   You want to allow a leading “ISBN” identifier, and ISBN parts can optionally
     be separated by hyphens or spaces

An ISBN is a unique identifier for commercial books and book-like products.
----------------------------------------
Algorithm :
An ISBN-13 check digit ranges from 0 to 9, and is computed using similar steps:

Multiply each of the first 12 digits by 1 or 3, alternating as you move from left to right, and sum the results.

Divide the sum by 10.

Subtract the remainder (not the quotient) from 10.

If the result is 10, use the number 0."""


def new_digit(num: int) -> int:
    num = [int(x) for x in str(num)[::-1]]
    for i in range(0,len(num),2):
        num[i] *= 3
    return num[::-1]

def conversion(num: str) -> int:
    return int(num.replace("-","")) 

def check_digit(num: str) -> int :
    num = conversion(num)
    check_digit = sum(new_digit(num//10)) % 10
    return 0 if check_digit == 0 else 10 - check_digit

print(check_digit("978-0-596-52068"))




    

