import sys

def gen_powers(power: int) ->list[int]:
    bin_num = bin(power)[2:]
    binary = [(2**int(i))*int(bin_num[i]) for i in range(len(bin_num))]
    return binary

def compute_parts(base: int,power: list) ->list[int]:
    powers= []
    for i in power:
        powers.append(base**i)
    return powers

def final(base: int,power: int) -> int:
    powers = gen_powers(power)
    parts = compute_parts(base,powers)
    res = 1
    for part in parts:
        res *= part
    return res
print(final(2,51))
