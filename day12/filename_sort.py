from collections import defaultdict as ddict
data = ["day001" , "day002" , "day003"]

def file_sort(filenames: list[str] ) -> list[str] :

    return sorted(filenames, key=lambda x: int(x.split(' ') ))

def file_num(filename):
    return "".join([x for x in filename if x.isnumeric()])
print(file_sort(data))