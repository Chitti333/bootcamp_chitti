from collections import defaultdict as ddict
import sys

freq = ddict(list)
for i, arg in enumerate(sys.argv[1:]):
    for ch in open(arg).read():
        if ch not in freq:
            freq[ch] = [0,0,0]
        else:
            freq[ch][i] += 1

for ch in freq:
    print(f"{ch} {freq[ch][0]:5} {freq[ch][1]:5} {freq[ch][2]:3}")
