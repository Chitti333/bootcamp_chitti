import random
import csv
from collections import defaultdict as ddict
ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
freq = ddict()
def load_data(filename: str) :
    with open(filename, mode ='r')as file:
        lines = file.readlines()
    lines = [line.strip().split(",") for line in lines[1:]]
    return {ch: float(per.replace("%" , "e-2")) for ch, per in lines}
    
data = load_data("letter_frequencies.csv")

random.seed(42)
def uni(strlen: int) -> str :
    out = ""
    for i in range(strlen):
        idx = int(random.random() * 100) %len(ALPHA)
        out += ALPHA[idx]
    for _ in out:
        freq[_] = (out.count(_) / strlen )* 100 
    for i in freq:
        print(i,freq[i])
print(uni(100))


