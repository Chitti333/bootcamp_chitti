from collections import Counter
def load_data(filename: str) -> str:
  file = open(filename)
  return "".join([x for x in file.readlines()]).replace("\n" ," ")


data = load_data("mono_ciph020.txt")
data1 = data.split(" ")
data2 = data1.replace("p")
most = Counter(data1)
res = most.most_common(8)
print
