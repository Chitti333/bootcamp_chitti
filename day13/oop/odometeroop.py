DIGITS = '123456789'

def min_max(size: int) -> tuple[int , int]:
    return int(DIGITS[:size]) ,int(DIGITS[-size:])

class OdoMeter:
    def __init__(self,size) :
        self.size = size
        self.reading = min_max(size)[0]
        self.min_reading,self.max_reading = min_max(size)[0],min_max(size)[1]

    def __str__(self):  
        return f'<{self.reading}>'
    
    def __repr__(self):   #inspects the vars
        size = len(str(self.reading))
        return f'Size: {size} , Reading: {self.reading}'
    def reading(self) -> int:
        return self.reading
    @classmethod
    def is_ascending(cls,reading) -> bool:
        return all(a<b for a,b in zip(str(reading),str(reading)[1]))
    
    def __lt__(self,other) -> bool: #if u want to change the dunder methods you are free to do that but face the consequences
        if self.size(self.reading) != other.reading:
            raise ValueError("Odometers of diff sizes are incomparable")
        return self.reading < other.reading
    

    def forward(self,step : int = 1) -> None:   #self is the own state of the odometer
        for i in range(step):
            if self.reading == self.max_reading :
                self.reading = self.min_reading
            else:
                self.reading += 1
                while not OdoMeter.is_ascending(self.reading):
                    self.reading += 1
    def backward(self,step: int = 1):
        for i in range(step):
            if self.reading == self.min_reading:
                self.reading = self.max_reading
            else:
                self.reading -= 1
                while not OdoMeter.is_ascending(self.reading):
                    self.reading -= 1
        

o1 = OdoMeter(3)
o1.forward()
print(o1)
o2 = OdoMeter(3)
o2.backward()
print(o2)
