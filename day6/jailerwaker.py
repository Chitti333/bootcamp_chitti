def cells_state(limit: int) :
    cells = [True for i in range(limit+1)]
    p = 1
    while p <= limit:
        for i in range(p,limit+1, p):
            cells[i] = not cells[i]
        p += 1
    return  [p+1 for p in range(limit) if not cells[p] ]

print(cells_state(100))

#1-cell[i]