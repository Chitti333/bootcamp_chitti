def new_num(num):
    num = [int(x) for x in str(num)[::-1]]
    for i in range(0,len(num),2):
        num[i] *= 2
        num[i] = digit_sum(num[i])
    return num[::-1]
    
def digit_sum(digit):
    return 1 + (digit -1) % 9

def check_digit(num):
    n = sum(new_num(num))
    return 10 - (n % 10)

def is_valid(num: int) ->bool :
    return num % 10 == check_digit(num//10)

print(check_digit(1789372997))
print(new_num(1789372997))

print(is_valid(17893729978))