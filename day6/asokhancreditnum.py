def digit_sum(n: int) -> int:
    return 1 + (n-1) %9

def new_digits(n: int) -> int:
    def new_digit(i: int,d: str) ->int:
        if i% 2 == 0 :
            return digit_sum(int(d+d))
        else:
            return int(d)
    return [new_digit(*e) for e in enumerate(reversed(str(n)))][::-1] #if someone needs to use it further we should return the corct num

def check_digit(n: int) -> int:
    return 10 - (sum(new_digits(n)) % 10)

print(new_digits(1789372997))

def is_valid(num: int) ->bool :
    
    return num % 10 == check_digit(num//10)

print(is_valid(17893729977))

