#print(1071%462)
#print(462 % 147)
#print(147 % 21)

def gcd(n1,n2):
    min_digit,max_digit = min(n1,n2) , max(n1,n2)
    while min_digit != 0:
        temp= min_digit
        min_digit = max_digit % min_digit
        max_digit = temp
    return max_digit

print(gcd(1071,462))