PLUS = "+"
MINUS = '-'
SPACE = ' '
CARET = "^"

def getvar(poly):
    for ch in poly:
        if ch.isaplha():
            return ch
def terms(poly):
    poly = ''.join(ch for ch in poly if ch != SPACE)
    if poly[0].isdigit():
        poly = PLUS + poly
    poly = poly.replace(MINUS,PLUS +MINUS)

    return poly.split(PLUS)[1::]

poly = "3x^3 + 3x^2 - 3x"
print(terms(poly))