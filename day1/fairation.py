def fair_rations(arr):
    additional_rations = 0
    n = len(arr)
    odd_person = False
    for person in arr:
        if person % 2 != 0:
            if odd_person: 
                person += 1  
                additional_rations += 2
                odd_person = False  
            else:
                odd_person = True  
    if odd_person: 
        return "NO"
    return additional_rations


arr = [4, 5, 3, 2, 1, 5, 7]
result = fair_rations(arr)
print(result)
