def fair_rations(inp):
    add_rations = 0
    n = len(inp)
    for i in range(n-1):
        if isodd(inp[i]):
            inp[i] += 1
            inp[i+1] += 1
            add_rations += 2
    if all( x % 2 == 0 for x in inp):
        return add_rations
    else:
        return "No"
def isodd(ele):
    return ele % 2 == 1
print(fair_rations([11,22,33,45,53]))