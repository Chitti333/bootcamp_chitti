SPACE = ' '
def clean(poly):
    return ''.join(ch for ch in poly if ch != SPACE)
def terms(poly):
    return clean(poly).replace('+','$').replace('-','$-').split("$")
def new(poly):
    return '+'.join(reversed(terms(poly))).replace('+-','-')
print(new("2x^4 + 3x^3 - 4x^2 - 2x +7"))
