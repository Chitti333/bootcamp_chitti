from collections import defaultdict as ddict
COMMENT = "#"
SPACE = " "

def load_data(filename: str) -> dict[int:list[str]] :
     return [parse(line) for line in open(filename) if line[0] != COMMENT]

def find_seperator(line: str) ->int:
    for ch in line:
        if ch.isdigit():
            return int(line.find(ch))
        
def parse(line: str) -> tuple[int,int,str,list[int]]:
    line = line.strip()
    marks_index = find_seperator(line)
    name,marks  = line[:(marks_index)] ,list(map(int,line[marks_index:].split()))
    fails = len([mark for mark in marks if mark < 40])

    return fails,sum(marks),name,marks

def make_ranklist(filename: str) -> list[str]:
    ranklist = []
    data = sorted(load_data(filename), key=lambda rec: (rec[0], -rec[1]))
    rank = 0
    prev = 0
    for position, rec in enumerate(data, start=1):
        if rec[1] != prev:
            rank = position
            prev = rec[1]
        ranklist.append(f'{rank:4}{rec[1]:5}  {rec[2]:40}{rec[3]}')
    return ranklist
for line in make_ranklist("data.txt"):
    print(line)

