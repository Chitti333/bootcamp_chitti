import math
def isprime(num: int) -> bool:
    max = int(math.sqrt(num))
    return len([i for i in range(2,max) if num % i == 0]) == 0

def primes(limit):

    primes  = [i for i in range(2,limit) if isprime(i)]
    return primes

print(primes(1000))
    