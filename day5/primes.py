def primes(limit: int) -> list[int]:
    def is_prime(n: int) -> bool:
        factor = 5
        while factor * factor  <= n:
            if n % factor == 0 :
                return False
            factor += 2
        return True
    
    primes = [2,3,5]

    for n in range(30,limit,30):
    
        for step in [1,-1,11,-11,17,-17,-19,19,23,-23]:
            print(n + step)
            if is_prime(n + step):
                primes.append(n+step)  
    return sorted(primes)

    for n in range(6,limit,6):
        #i += 1
        #if i % 4 ==0:
            #continue
        for step in [1,-1]:
            print(n + step)
            if is_prime(n + step):
                primes.append(n+step)  
    return primes,len(primes)

print(primes(1000))      

